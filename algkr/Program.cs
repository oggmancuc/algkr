﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algkr
{
    class Program
    {
        public class Vertex // вершина
        {
            public string Name { get; set; }
            public int Distance { get; set; }
            public Vertex Parent { get; set; }
        }

        public class Edge // ребро
        {
            public int U { get; set; }
            public int V { get; set; }
            public int Weight { get; set; }
        }

        public static string[] Cities = {
            "Київ",
            "Житомир",
            "Біла церква",
            "Прилуки",
            "Новоград-Волинський",
            "Бердичів",
            "Шепетівка",
            "Умань",
            "Черкаси",
            "Полтава",
            "Суми",
            "Миргород",
            "Рівне",
            "Вінниця",
            "Кременчук",
            "Харків",
            "Луцьк",
            "Хмельницький",
            "Тернопіль",
        };
            public static int[,] AdjacencyMatrix = { // матриця суміжності
                {0, 135, 78, 128,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0, 80, 38, 115,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0, 115, 146, 181,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0, 175, 109,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0, 100,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0, 73,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0, 105,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0, 130,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0, 68,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0, 110,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0, 104},
                {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            };

        public static bool[] visited = new bool[AdjacencyMatrix.GetLength(0)]; // масив пройдених вершин
        public static List<int> list = new List<int>(); // стек
        public static void DFS(int f, int sum = 0)
        {
            if (!visited[f])
            {
                visited[f] = true; // позначка, що вершину пройдено
                list.Add(f);
                for (int i = 0; i < AdjacencyMatrix.GetLength(0); i++) // прохід по вершинам
                {
                    if (AdjacencyMatrix[f, i] != 0) // не виконємо якщо відстань 0
                    {
                        sum += AdjacencyMatrix[f, i]; // додавання відстані до результату
                        DFS(i, sum);
                        Console.Write("Шлях: ");
                        foreach (int city in list)
                        {
                            Console.Write($"{Cities[city]} -- ");
                        }
                        Console.Write($": {sum} km\n");
                        sum -= AdjacencyMatrix[f, i]; // віднімання відвіданої відстані
                        list.RemoveAt(list.Count - 1); // видалення відвіданої вершини
                    }
                }
            }
        }

        public static void InitializeSingleSource(Vertex[] vertices, Vertex s) // встановленя завищеної відстані від елементів масиву вершин до source-вершини 
        {
            foreach (Vertex v in vertices)
            {
                v.Distance = int.MaxValue;
                v.Parent = null;
            }
            s.Distance = 0; // позначаємо source-вершину
        }

        public static void Relax(Vertex u, Vertex v, int weight) // "релаксуємо" вершину - встановлюємо мінімальну відстань та назначаємо предка
        {
            if (v.Distance > u.Distance + weight)
            {
                v.Distance = u.Distance + weight;
                v.Parent = u;
            }
        }

        public static bool BellmanFord(Vertex[] vertices, List<Edge> edges, int source) // алгоритм
        {
            InitializeSingleSource(vertices, vertices[source]); // ініціалізуємо вершини
            for (int i = 0; i < vertices.Length - 1; i++) // релаксуємо кожне ребро 
            {
                foreach (Edge edge in edges)
                {
                    Relax(vertices[edge.U], vertices[edge.V], edge.Weight);
                }
            }
            foreach (Edge edge in edges) // перевіряємо, чи існує шлях до всіх вершин з source-вершини чи ні 
            {
                Vertex u = vertices[edge.U];
                Vertex v = vertices[edge.V];
                if (v.Distance > u.Distance + edge.Weight)
                {
                    return false;
                }
            }
            return true;
        }

        public static void PrintPath(Vertex u, Vertex v, Vertex[] vertices) // вивід результатів виконання алгоритму
        {
            if (v != u)
            {
                PrintPath(u, v.Parent, vertices);
                Console.WriteLine($" ---> {v.Name}: {v.Distance} км --->");
            }
            else
                Console.WriteLine($"Найкоротша відстань від {v.Name} до --->"); // source-вершина
        }

        public static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            Vertex[] vertices = new Vertex[AdjacencyMatrix.GetLength(0)];
            List<Edge> edges = new List<Edge>();
            for (int i = 0; i < AdjacencyMatrix.GetLength(0); i++)
            {
                Console.WriteLine($"{i}: {Cities[i]}");
            }
            Console.WriteLine(" --- ");
            DFS(0); // виводжу шляхи
            Console.WriteLine(" --- ");
            Console.Write("Оберіть початкове місто: ");
            int source = int.Parse(Console.ReadLine()); // source-вершина
            Console.Write("Оберіть кінцеве місто (допустиме серед показаних шляхів): ");
            int end = int.Parse(Console.ReadLine());
            Console.WriteLine(" --- ");
            for (int i = 0; i < vertices.Length; i++) // встановлюю назви вершин
                vertices[i] = new Vertex() { Name = Cities[i] };
            Queue<int> verticesQueue = new Queue<int>(); 
            verticesQueue.Enqueue(source);
            while (verticesQueue.Count > 0) // додаю всі ребра у чергу, починаючи з source-вершини
            {
                int u = verticesQueue.Dequeue();
                for (int v = 0; v < AdjacencyMatrix.GetLength(0); v++)
                {
                    if (AdjacencyMatrix[u, v] > 0) // додаю ребро, якщо воно має величину
                    {
                        edges.Add(new Edge() { U = u, V = v, Weight = AdjacencyMatrix[u, v] });
                        verticesQueue.Enqueue(v);
                    }
                }
            }
            BellmanFord(vertices, edges, source); // викликаю алгоритм
            PrintPath(vertices[source], vertices[end], vertices); // виводжу результати
            Console.ReadLine();
        }
    }
}
